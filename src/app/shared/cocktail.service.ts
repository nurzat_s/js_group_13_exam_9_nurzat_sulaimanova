import { Subject } from 'rxjs';
import { Cocktail } from './cocktail.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map, tap} from 'rxjs/operators';

@Injectable()

export class CocktailService {
  cocktailsChange = new Subject<Cocktail[]>();
  cocktailsFetching = new Subject<boolean>();
  cocktailUploading = new Subject<boolean>();
  cocktailRemoving = new Subject<boolean>();

  private cocktails: Cocktail[] = [];

  constructor(private http: HttpClient) {}

  fetchCocktails() {
    this.cocktailsFetching.next(true);
    this.http.get<{[id: string]: Cocktail}>('https://plovo-e531e-default-rtdb.firebaseio.com/cocktails.json')
      .pipe(map(result => {
        if(result === null) {
          return [];
        }
        return Object.keys(result).map(id => {
          const cocktailData = result[id];
          return new Cocktail(id, cocktailData.name, cocktailData.imageUrl, cocktailData.type, cocktailData.description,
            cocktailData.ingredients, cocktailData.prepDescription);
        });
      }))
      .subscribe(cocktails => {
        this.cocktails = cocktails;
        this.cocktailsChange.next(this.cocktails.slice());
        this.cocktailsFetching.next(false);
      }, error => {
        this.cocktailsFetching.next(false);
      });
  }

  getCocktails() {
    return this.cocktails.slice();
  }

  fetchCocktail(id: string) {
    return this.http.get<Cocktail | null>(`https://plovo-e531e-default-rtdb.firebaseio.com/cocktails/${id}.json`).pipe(
      map(result => {
        if(!result) {
          return null;
        }
        return new Cocktail(id, result.name, result.imageUrl, result.type, result.description,
          result.ingredients, result.prepDescription);
      })
    );
  }

  addCocktail(cocktail: Cocktail) {
    const body = {
      name: cocktail.name,
      imageUrl: cocktail.imageUrl,
      type: cocktail.type,
      description: cocktail.description,
      ingredients: cocktail.ingredients,
      prepDescription: cocktail.prepDescription
    };

    this.cocktailUploading.next(true);

    return this.http.post('https://plovo-e531e-default-rtdb.firebaseio.com/cocktails.json', body).pipe(
      tap(() => {
        this.cocktailUploading.next(false);
      }, () => {
        this.cocktailUploading.next(false);
      })
    );
  }




}
