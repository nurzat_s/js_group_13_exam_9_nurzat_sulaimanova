import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormArray, FormControl, FormGroup, NgForm, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { CocktailService } from '../shared/cocktail.service';
import { Router } from '@angular/router';
import { Cocktail } from '../shared/cocktail.model';
import { urlValidator } from '../validate-url.directive';

@Component({
  selector: 'app-new-cocktail',
  templateUrl: './new-cocktail.component.html',
  styleUrls: ['./new-cocktail.component.css']
})
export class NewCocktailComponent implements OnInit, OnDestroy{
  @ViewChild('f') form!: NgForm;
  cocktailForm!: FormGroup;

  isUploading = false;
  cocktailUploadingSubscription!: Subscription;

  constructor(private cocktailService: CocktailService, private router: Router) { }

  ngOnInit(): void {
    this.cocktailForm = new FormGroup({
      name: new FormControl('', Validators.required),
      imageUrl: new FormControl('', [Validators.required, urlValidator]),
      type: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),
      ingredients: new FormArray([]),
      prepDescription: new FormControl('', Validators.required)
    });

    this.cocktailUploadingSubscription = this.cocktailService.cocktailUploading.subscribe((isUploading: boolean) => {
      this.isUploading = isUploading;
    })
  }

  onSubmit() {
    console.log(this.cocktailForm);
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.cocktailForm.get(fieldName);
    return Boolean(field && field.touched && field.errors?.[errorType]);
  }

  addIngredient() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    const ingredientsGroup = new FormGroup({
      ingredientName: new FormControl('', Validators.required),
      measure: new FormControl('', Validators.required),
      quantity: new FormControl('', Validators.required),
    })
    ingredients.push(ingredientsGroup);
  }

  removeIngredient(index: number) {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    ingredients.removeAt(index);
  }

  getIngredientControls() {
    const ingredients =  <FormArray>this.cocktailForm.get('ingredients');
    return ingredients.controls;
  }

  saveCocktail() {
    const id = Math.random().toString();

    const cocktail = new Cocktail(
      id,
      this.form.value.name,
      this.form.value.imageUrl,
      this.form.value.type,
      this.form.value.description,
      this.form.value.ingredients,
      this.form.value.prepDescription
    );


    const next = () => {
      void this.router.navigate(['/']);
    };

    this.cocktailService.addCocktail(cocktail).subscribe(next);
  }

  ngOnDestroy() {
    this.cocktailUploadingSubscription.unsubscribe();
  }


}
