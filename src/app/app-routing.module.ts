import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CocktailsComponent } from './cocktails/cocktails.component';
import { CocktailResolverService } from './cocktails/cocktail-resolver.service';
import { NewCocktailComponent } from './new-cocktail/new-cocktail.component';


const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'new', component: NewCocktailComponent},
  // {path: 'view', component: RecipeDetailsComponent},
  {path: 'cocktail', component: CocktailsComponent, children: [
      {
        path: ':id',
        component: CocktailsComponent,
        resolve: {
          cocktail: CocktailResolverService
        }
      },
    ]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
