import { Injectable } from '@angular/core';
import { Cocktail } from '../shared/cocktail.model';
import { ActivatedRouteSnapshot, Resolve, Router, RouterStateSnapshot } from '@angular/router';
import { CocktailService } from '../shared/cocktail.service';
import { EMPTY, mergeMap, Observable, of } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CocktailResolverService implements Resolve<Cocktail>{

  constructor(private cocktailService: CocktailService, private router: Router) {
  }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Cocktail> | Observable<never> {
    const cocktailId = <string>route.params['id'];
    return this.cocktailService.fetchCocktail(cocktailId).pipe(mergeMap(cocktail => {
      if (cocktail) {
        return of(cocktail);
      } else {
        void this.router.navigate(['/recipes']);
        return EMPTY;
      }
    }));
  }
}
