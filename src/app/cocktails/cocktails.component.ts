import { Component, OnInit } from '@angular/core';
import { Cocktail } from '../shared/cocktail.model';
import { Subscription } from 'rxjs';
import { CocktailService } from '../shared/cocktail.service';

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.css']
})
export class CocktailsComponent implements OnInit {
  cocktails!: Cocktail[];
  cocktailsChangeSubscription!: Subscription;
  cocktailsFetchingSubscription!: Subscription;
  isFetching: boolean = false;


  constructor(private cocktailService: CocktailService) { }

  ngOnInit(): void {
    this.cocktails = this.cocktailService.getCocktails();
    this.cocktailsChangeSubscription = this.cocktailService.cocktailsChange.subscribe((cocktails: Cocktail[]) => {
      this.cocktails = cocktails;
    })
    this.cocktailsFetchingSubscription = this.cocktailService.cocktailsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.cocktailService.fetchCocktails();
  }


  ngOnDestroy() {
    this.cocktailsChangeSubscription.unsubscribe();
    this.cocktailsFetchingSubscription.unsubscribe();
  }
}
