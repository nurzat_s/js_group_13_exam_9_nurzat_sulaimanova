import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Directive } from '@angular/core';

export const urlValidator = (control: AbstractControl): ValidationErrors | null => {
  // control.value
  const symbols = /^[ht\tp:/\/]/.test(control.value);
  const symbols1 = /^[ht\tps:/\/]/.test(control.value);

  if (symbols || symbols1) {
    return null;
  }

  return {url: true};
};

@Directive({
  selector: '[appUrl]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateUrlDirective,
    multi: true
  }]
})
export class ValidateUrlDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return urlValidator(control);
  }
}
